import * as actions from './SegmentationActionConstants.jsx'


// -------- Mask save/load --------

export const maskSaveData = (p_mask_data) => ({
	type: actions.MASK_SAVE_DATA,
	mask_data: p_mask_data
})

export const maskUndo = () => ({
	type: actions.MASK_UNDO
})

export const maskRedo = () => ({
	type: actions.MASK_REDO
})

export const maskClean = () => ({
	type: actions.MASK_CLEAN
})
