// React
import {
	Component,
	createRef
} from 'inferno'

// Redux
import {
	connect
} from 'inferno-redux'

// Actions
import * as SegmentationActions from './SegmentationActions.jsx'

// Style
import './SegmentationStyle.scss'

// -------- Creating the Presentational Component --------

class SegmentationComponent extends Component
{
	constructor(props)
	{
		super(props)

		this.refs = {}

		this.current_image = {
			box: null,
			brush_data: null,
			context: null,
			data: null,
			drag: null,
			drag_should_save: false,
			image: new Image(),
			mask: null,
			temp_mask_data: null,
			object: null,
			overlay_context: null
		}

		this.tools = {
			threshold: 1
		}

		this.current_image.image.onload = () => {
			const image_width = this.current_image.image.width
			const image_height = this.current_image.image.height
			if((this.props.masks || []).length)
			{
				this.props.maskClean()
			}
			this.refs.current_image.width = image_width
			this.refs.current_image.height = image_height
			this.refs.overlay.width = image_width
			this.refs.overlay.height = image_height
			this.current_image.box = this.refs.current_image.getBoundingClientRect()
			this.current_image.context.drawImage(this.current_image.image, 0, 0)
			this.current_image.data = this.current_image.context.getImageData(
				0, 0, image_width, image_height)
			// Clear previous mask if any
			if(this.current_image.mask !== null)
			{
				delete this.current_image.mask
			}
			if(this.clear_current_image.temp_mask_data !== null)
			{
				delete this.current_image.temp_mask_data
			}
			this.current_image.mask = new ImageData(image_width, image_height);
			this.current_image.temp_mask_data = new Uint8ClampedArray(image_width * image_height)

			if(this.current_image.brush_data === null)
			{
				this.current_image.brush_data = this.current_image.context.createImageData(1, 1)
				this.current_image.brush_data.data[0] = 220
				this.current_image.brush_data.data[1] = 85
				this.current_image.brush_data.data[2] = 85
				this.current_image.brush_data.data[3] = 255
			}
			this.current_image.context.lineWidth = 1
			this.current_image.context.lineCap = 'square'
			this.current_image.context.strokeStyle = 'rgb(220, 85, 85)'
		}

		this.clear_current_image = () => {
			if(this.current_image.object !== null)
			{
				window.URL.revokeObjectURL(this.current_image.object)
				this.current_image.object = null
			}
		}
		this.open_file = (p_event) => {
			const files = p_event.target.files
			if(files && files[0])
			{
				this.current_image.object = window.URL.createObjectURL(files[0])
				this.current_image.image.src = this.current_image.object
			}
		}

		// -------- Canvas events --------

		this.clear_mask = () => {
			this.current_image.mask.data.fill(0)
			this.props.maskSaveData(this.current_image.mask.data)
			this.current_image.overlay_context.putImageData(this.current_image.mask, 0, 0)
		}

		this.undo = () => {
			this.current_image.overlay_context.clearRect(0, 0, this.refs.overlay.width, this.refs.overlay.height)
			this.props.maskUndo()
			if(this.props.mask_current >= 0)
			{
				this.current_image.mask.data.set(this.props.masks[this.props.mask_current])
				this.current_image.overlay_context.putImageData(this.current_image.mask, 0, 0)
			}
			else
			{
				this.current_image.mask.data.fill(0)
			}
		}

		this.redo = () => {
			this.current_image.overlay_context.clearRect(0, 0, this.refs.overlay.width, this.refs.overlay.height)
			this.props.maskRedo()
			if(this.props.mask_current >= 0)
			{
				this.current_image.mask.data.set(this.props.masks[this.props.mask_current])
				this.current_image.overlay_context.putImageData(this.current_image.mask, 0, 0)
			}
		}

		this.smooth_pixels = () => {
			let pixel_x = 0
			let pixel_y = 0
			while(pixel_y <= this.current_image.data.height)
			{
				while(pixel_x <= this.current_image.data.width)
				{
					pixel_x += 1
				}
				pixel_y += 1
			}
		}

		this.pixel_check = (x, y, color) => {
			const pixel_index  = x + (y * this.current_image.data.width)
			if(this.current_image.temp_mask_data[pixel_index] !== 0)
			{
				return false
			}
			const index = 4 * pixel_index
			const pixel_color = [
				this.current_image.data.data[index],
				this.current_image.data.data[index + 1],
				this.current_image.data.data[index + 2]
			]
			return Math.sqrt(
				(Math.pow(pixel_color[0] - color[0], 2)
				 + Math.pow(pixel_color[1] - color[1], 2)
				 + Math.pow(pixel_color[2] - color[2], 2)) / 3) <= this.tools.threshold
		}

		this.pixel_select = (x, y) => {
			const pixel_index  = x + (y * this.current_image.data.width)
			const index = 4 * pixel_index
			const checker_width = 5
			const color = 63 + (((Math.floor(x / checker_width) % 2) + (Math.floor(y / checker_width) % 2)) % 2) * 127
			this.current_image.temp_mask_data[pixel_index] = 1
			this.current_image.mask.data[index] = color + 40
			this.current_image.mask.data[index + 1] = color - 40
			this.current_image.mask.data[index + 2] = color - 40
			this.current_image.mask.data[index + 3] = 127
		}

		this.scanline_fill = (x, y, color) => {
			this.pixel_select(x, y)
			this.current_image.temp_mask_data.fill(0)
			let stack = [{
				start_x: x,
				end_x: x + 1,
				y: y,
				dir: 0,
				scan_left: true,
				scan_right: true
			}]
			let current_segment = null
			let start_x = 0
			let end_x = 0
			while(stack.length > 0)
			{
				current_segment = stack.pop()
				start_x = current_segment.start_x
				end_x = current_segment.end_x
				if(current_segment.scan_left)
				{
					while((start_x > 0) && this.pixel_check(start_x - 1, current_segment.y, color))
					{
						start_x -= 1
						this.pixel_select(start_x, current_segment.y)
					}
				}
				if(current_segment.scan_right)
				{
					while((end_x < this.current_image.data.width) && this.pixel_check(end_x, current_segment.y, color))
					{
						this.pixel_select(end_x, current_segment.y)
						end_x += 1
					}
				}
				// at this point, the segment from start_x (inclusive) to end_x (exclusive) is filled. compute the region to ignore
				current_segment.start_x -= 1 // since the segment is bounded on either side by filled cells or array edges, we can extend the size of
				current_segment.end_x += 1 // the region that we're going to ignore in the adjacent lines by one
				// scan above and below the segment and add any new segments we find
				if(current_segment.y > 0)
				{
					this.scanline_addline(
						stack, start_x, end_x, current_segment.y - 1,
						current_segment.start_x, current_segment.end_x, -1, current_segment.dir <= 0, color)
				}
				if(current_segment.y < this.current_image.data.height - 1)
				{
					this.scanline_addline(
						stack, start_x, end_x, current_segment.y + 1,
						current_segment.start_x, current_segment.end_x, 1, current_segment.dir >= 0, color)
				}
			}
		}

		this.scanline_addline = (stack, start_x, end_x, y, ignore_start, ignore_end, dir, is_next_in_dir, color) => {
			let region_start = -1
			let x = start_x
			while(x < end_x) // scan the width of the parent segment
			{
				// if we're outside the region we should ignore and the cell is clear
				if((is_next_in_dir || x < ignore_start || x >= ignore_end) && this.pixel_check(x, y, color))
				{
					this.pixel_select(x, y)
					if(region_start < 0)
					{
						region_start = x // start a new segment if we haven't already
					}
				}
				else if(region_start >= 0)
				{
					stack.push({
						start_x: region_start,
						end_x: x,
						y: y,
						dir: dir,
						scan_left: region_start == start_x,
						scan_right: false
					})
					region_start = -1 // push the segment and end it
				}
				x += 1
				if(!is_next_in_dir && (x < ignore_end) && (x > ignore_start))
				{
					x = ignore_end - 1 // skip over the ignored region
				}
			}
			if(region_start >= 0)
			{
				stack.push({
					start_x: region_start,
					end_x: x,
					y: y,
					dir: dir,
					scan_left: region_start == start_x,
					scan_right: true
				})
			}
		}

		this.canvas_click = (p_event) => {
			if(p_event.button === 0) // if left click pressed for this event (button = event trigger)
			{
				const mouse_event = {
					x: Math.floor(p_event.clientX - this.current_image.box.left + window.scrollX - .5),
					y: Math.floor(p_event.clientY - this.current_image.box.top + window.scrollY - .5)
				}
				// this.current_image.context.putImageData(this.current_image.brush_data, mouse_event.x, mouse_event.y)
				// this.current_image.drag = mouse_event
				// const target_pixel = this.current_image.context.getImageData(Math.floor(mouse_event.x), Math.floor(mouse_event.y), 1, 1)
				const index = 4 * (mouse_event.x + (mouse_event.y * this.current_image.data.width))
				const target_color = [
					this.current_image.data.data[index],
					this.current_image.data.data[index + 1],
					this.current_image.data.data[index + 2]
				]
				// console.log('target color : ', mouse_event, target_color)
				this.scanline_fill(mouse_event.x, mouse_event.y, target_color)
				this.current_image.overlay_context.putImageData(this.current_image.mask, 0, 0)
				this.props.maskSaveData(this.current_image.mask.data)
				this.current_image.drag = mouse_event
			}
			else
			{
				this.current_image.drag = null
			}
		}

		this.canvas_drag = (p_event) => {
			if(this.current_image.drag !== null)
			{
				if(p_event.buttons === 1) // if left button is pressed during the event (buttons = state)
				{
					const mouse_event = {
						x: Math.floor(p_event.clientX - this.current_image.box.left + window.scrollX - .5),
						y: Math.floor(p_event.clientY - this.current_image.box.top + window.scrollY - .5)
					}
					// this.current_image.context.beginPath()
					// this.current_image.context.moveTo(this.current_image.drag.x, this.current_image.drag.y)
					// this.current_image.context.lineTo(mouse_event.x, mouse_event.y)
					// this.current_image.context.closePath()
					// this.current_image.context.stroke()
					const index = 4 * (mouse_event.x + (mouse_event.y * this.current_image.data.width))
					const target_color = [
						this.current_image.data.data[index],
						this.current_image.data.data[index + 1],
						this.current_image.data.data[index + 2]
					]
					// console.log('target color : ', mouse_event, target_color)
					this.scanline_fill(mouse_event.x, mouse_event.y, target_color)
					this.current_image.overlay_context.putImageData(this.current_image.mask, 0, 0)
					this.current_image.drag = mouse_event
					this.current_image.drag_should_save = true
				}
				else
				{
					this.current_image.drag = null
					if(this.current_image.drag_should_save)
					{
						this.current_image.drag_should_save = false
						this.props.maskSaveData(this.current_image.mask.data)
					}
				}
			}
		}

		// -------- Events --------

		this.threshold_slider_change = (p_event) => {
			this.tools.threshold = Math.floor(256 * ((Math.exp(p_event.target.value / 100) - 1) / (Math.exp(1) - 1)))
			this.refs.threshold_input.value = this.tools.threshold
		}

		this.threshold_input_input = (p_event) => {
			const value = p_event.target.value > 256 ? 256 : p_event.target.value
			this.tools.threshold = value
			this.refs.threshold_slider.value = 100 * Math.log(1 + ((value * (Math.exp(1) - 1)) / 256))
		}

		this.threshold_input_change = (p_event) => {
			this.refs.threshold_input.value = this.tools.threshold
		}
	}

	componentDidMount(domNode)
	{
		console.log("componentDidMount")
		this.refs.current_image.width = 0
		this.refs.current_image.height = 0
	}

	componentDidUnmount(domNode)
	{
		console.log("componentDidUnmount")
		this.clear_current_image()
	}

	render()
	{
		return (
			<div id="segmentation-container">
				<label htmlFor="file" className="folder-button">Open File</label>
				<input id="file" type="file" className="folder-input" onChange={this.open_file} />
				<label htmlFor="folder" className="folder-button">Open Folder</label>
				<input id="folder" type="file" className="folder-input" webkitdirectory />
				<input type="range" min="1" max="100" step="1"
						 ref={(node) => {this.refs.threshold_slider = node}}
						 onChange = {this.threshold_slider_change}
				/>
				   <input type="number" min="1" max="256"
                      onInput = {this.threshold_input_input}
                      onChange = {this.threshold_input_change}
                      ref={(node) => {this.refs.threshold_input = node}}
                  defaultValue={this.tools.threshold} />
				<button onClick={this.undo}>Undo</button>
				<button onClick={this.redo}>Redo</button>
				<span>{'History : ' + (this.props.masks || []).length}</span>
				<button onClick={this.clear_mask}>Clear</button>
				<div className="canvas-container">
					 <canvas className="main-canvas"
						  ref={(node) => {this.refs.current_image = node; this.current_image.context = node.getContext('2d')}}
						  onMouseDown={this.canvas_click}
						  onMouseMove={this.canvas_drag}>
					 </canvas>
					 <canvas className="overlay-canvas"
						  ref={(node) => {this.refs.overlay = node; this.current_image.overlay_context = node.getContext('2d')}}>
					 </canvas>
				</div>
			</div>
		)
	}
}

// -------- Creating the Container Component--------

const mapStateToProps = (state, ownProps) =>
{
	return {
		strings: state.root.strings,
		masks: state.root.masks,
		mask_current: state.root.mask_current
	}
}

const mapDispatchToProps = (dispatch) =>
{
	return {
		maskSaveData: (p_mask_data) => {
			dispatch(SegmentationActions.maskSaveData(p_mask_data))
		},
		maskUndo: () => { dispatch(SegmentationActions.maskUndo()) },
		maskRedo: () => { dispatch(SegmentationActions.maskRedo()) },
		maskClean: () => { dispatch(SegmentationActions.maskClean()) }
	}
}

const SegmentationContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(SegmentationComponent)

export default SegmentationContainer
