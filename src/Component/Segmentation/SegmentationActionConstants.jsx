export const MASK_SAVE_DATA = 'MASK_SAVE_DATA'
export const MASK_UNDO = 'MASK_UNDO'
export const MASK_REDO = 'MASK_REDO'
export const MASK_CLEAN = 'MASK_CLEAN'
