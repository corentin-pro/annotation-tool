`use strict`
import { Component, render } from 'inferno'
import { BrowserRouter, Route } from 'inferno-router';
import { Provider } from 'inferno-redux';
import { createStore, combineReducers, applyMiddleware} from 'redux';

// Middlewares
import {loggerMiddleware, promiseMiddleware} from './middlewares.jsx'

// Reducers
import SegmentationReducer from './Reducer/SegmentationReducer.jsx'

// Components
import SegmentationComponent from './Component/Segmentation/SegmentationComponent.jsx'

// Style
import './index.scss'

const initial_state = {
	root: {
		isDebug: process.env.NODE_ENV !== 'production',
		lang: localStorage.getItem('lang') || 'en',
		strings: {}
	}
}

const reducers = combineReducers({
	root: SegmentationReducer
})

const store = initial_state.root.isDebug ?
		createStore(reducers, initial_state, applyMiddleware(
			promiseMiddleware,
			loggerMiddleware // middleware that logs actions
		)) :
		createStore(reducers, initial_state ,applyMiddleware(
			promiseMiddleware
		))

// Render HTML on the browser
render(
	<Provider store={store}>
		<BrowserRouter>
			<Route name="Root" path="/" component={SegmentationComponent}/>
		</BrowserRouter>
	</Provider>
		, document.getElementById('root'))
