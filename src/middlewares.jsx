const loggerMiddleware = ({ getState }) =>
{
	return (next) => (action) => {
		console.log('will dispatch', action)

		// Call the next dispatch method in the middleware chain.
		let returnValue = next(action)

		console.log('state after dispatch', getState())

		// This will likely be the action itself, unless
		// a middleware further in chain changed it.
		return returnValue
	}
}

function createPromiseMiddleware(extraArgument)
{
	return ({ dispatch, getState }) => next => action =>
	{
		// Check if function (should return a promise)
		if (typeof action === 'function')
		{
			return action(dispatch, getState, extraArgument) // call the function (passing the useful dispatch and getState functions)
		}
		else
		{
			return next(action)
		}
	}
}
const promiseMiddleware = createPromiseMiddleware()
promiseMiddleware.withExtraArgument = createPromiseMiddleware


export {loggerMiddleware, promiseMiddleware}
