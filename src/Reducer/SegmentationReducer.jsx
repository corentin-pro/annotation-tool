import * as actions from '../Component/Segmentation/SegmentationActionConstants.jsx'

const SegmentationReducer = (state = {}, action) =>
{
	switch (action.type)
	{
		case actions.MASK_SAVE_DATA:
			const mask_current = (state.mask_current >= 0) ? state.mask_current : -1
			// if no current mask (first to be saved)
			if(mask_current === -1)
			{
				(state.masks || []).forEach((_, index) => { delete state.masks[index] })
				return Object.assign({}, state, {
					masks: [new Uint8ClampedArray(action.mask_data)],
					mask_current: 0
				})
			}
			let new_masks = (state.masks && state.masks.length) ? state.masks : []
			const max_masks = 10
			// delete masks to avoid until the array is below the max limit
			while(new_masks.length >= max_masks)
			{
				delete new_masks[0]
				new_masks = new_masks.slice(1, new_masks.length)
			}
			// delete masks after the current one (new history branch)
			while(new_masks.length > mask_current + 1)
			{
				delete new_masks[new_masks.length - 1]
				new_masks = new_masks.slice(0, new_masks.length - 1)
			}
			new_masks.push(new Uint8ClampedArray(action.mask_data))
			return Object.assign({}, state, {
				masks: new_masks,
				mask_current: new_masks.length - 1
			})
		case actions.MASK_UNDO:
			return Object.assign({}, state, {
				mask_current: (state.mask_current >= 0) ? state.mask_current - 1 : -1
			})
		case actions.MASK_REDO:
			return Object.assign({}, state, {
				mask_current: (state.masks && (state.mask_current < state.masks.length - 1)) ?
								  state.mask_current + 1 : state.mask_current
			})
		case actions.MASK_CLEAN:
			// delete all masks
			let masks = state.masks
			while((masks || []).length)
			{
				delete masks[0]
				masks = masks.slice(1, masks.length)
			}
			return Object.assign({}, state, {
				masks: null,
				mask_current: -1
			})
		default:
			return state
	}
}

export default SegmentationReducer
